# Flutter BLoC

Fait avec la librairie flutter_bloc (^4.0.0), le tutoriel suivi est sur la 1.0.0

Tutoriel : 'https://www.youtube.com/watch?v=hTExlt1nJZI'

Pas de grandes difficultés au vu de la différence de version, tout ce qu'il montre fonctionne tout aussi bien sur la 4.0.0. Il faut surtout faire attention aux paramètres de typage attendus (```<WeatherBloc>```) ici, ou ce genre de choses.

Il se peut aussi qu'il y ait une erreur du type 'Each child must be laid out exactly once', pour la régler, j'ai arrêté l'émulation, et j'ai lancé un 'flutter clean' avant de relancer.
