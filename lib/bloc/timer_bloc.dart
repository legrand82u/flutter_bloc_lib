import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc_v1_tutorial/data/model/clock.dart';

part 'timer_event.dart';
part 'timer_state.dart';

class TimerBloc extends Bloc<TimerEvent, TimerState> {
  final Clock _clock;
  TimerBloc(this._clock);

  @override
  TimerState get initialState => TimerInitial();

  @override
  Stream<TimerState> mapEventToState(
    TimerEvent event,
  ) async* {
    if (event is StartClock) {
      yield TimerStart(_clock);
      _clock.start();
      yield TimerRunning(_clock);
    } else if (event is StopClock) {
      _clock.stop();
      yield TimerStop(_clock);
    }
  }
}
