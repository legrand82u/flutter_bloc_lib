part of 'timer_bloc.dart';

abstract class TimerEvent extends Equatable {
  const TimerEvent();
}

class StartClock extends TimerEvent {
  @override
  List<Object> get props => [];
}

class StopClock extends TimerEvent {
  @override
  List<Object> get props => [];
}
