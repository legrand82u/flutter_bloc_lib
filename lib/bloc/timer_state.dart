part of 'timer_bloc.dart';

abstract class TimerState extends Equatable {
  const TimerState();
}

class TimerInitial extends TimerState {
  const TimerInitial();

  @override
  List<Object> get props => [];
}

class TimerStart extends TimerState {
  final Clock clock;

  TimerStart(this.clock);

  @override
  List<Object> get props => [clock];
}

class TimerRunning extends TimerState {
  final Clock clock;

  TimerRunning(this.clock);

  @override
  List<Object> get props => [
        clock,
        clock.count,
      ];
}

class TimerStop extends TimerState {
  final Clock clock;

  TimerStop(this.clock);

  @override
  List<Object> get props => [
        clock,
      ];
}
