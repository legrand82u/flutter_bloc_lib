import 'dart:isolate';
import 'dart:async';
// import 'dart:io';
// import 'dart:math';

import 'package:equatable/equatable.dart';

// class Clock {
//   int count = 0;

//   int getNumber() {
//     var random = Random();

//     count = random.nextInt(100);
//   }
// }

class Clock {
  ReceivePort receivePort;
  Isolate isolate;
  int count;

  void start() async {
    receivePort = ReceivePort();
    isolate = await Isolate.spawn(runTimer, receivePort.sendPort);
    receivePort.listen((data) {
      count = data;
      print('Réception : $count');
    });
  }

  static void runTimer(SendPort sendPort) {
    int count = 0;
    Timer.periodic(new Duration(seconds: 1), (Timer t) {
      count++;
      sendPort.send(count);
    });
  }

  void stop() {
    if (isolate != null) {
      isolate.kill();
      isolate = null;
    }
  }
}
