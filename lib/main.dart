import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_v1_tutorial/bloc/timer_bloc.dart';
import 'package:flutter_bloc_v1_tutorial/bloc/weather_bloc.dart';

import 'data/model/clock.dart';
import 'data/weather_repository.dart';
import 'pages/weather_search_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //Pour rentre un BloC global, on peut faire en sorte que son enfant soit "MaterialApp"
    //Peu utile, on ne veut pas qu'un bloc soit connu de 300 routes
    return MaterialApp(
        title: 'Weather App',
        debugShowCheckedModeBanner: false,
        //On oublie pas le "<WeatherBloc>"
        home: MultiBlocProvider(
          providers: [
            BlocProvider<WeatherBloc>(
              create: (context) => WeatherBloc(
                FakeWeatherRepository(),
              ),
            ),
            BlocProvider(
              create: (context) => TimerBloc(
                Clock(),
              ),
            )
          ],
          child: WeatherSearchPage(),
        ));
  }
}
