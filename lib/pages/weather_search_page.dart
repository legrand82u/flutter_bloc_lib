import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_bloc_v1_tutorial/bloc/timer_bloc.dart';
import 'package:flutter_bloc_v1_tutorial/bloc/weather_bloc.dart';
import 'package:flutter_bloc_v1_tutorial/data/model/weather.dart';

import 'weather_detail_page.dart';

class WeatherSearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Weather Search"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(vertical: 16),
            alignment: Alignment.center,
            //On ajoute un bloc Listener, pour ajouter une Snackbar, on ne la met pas dans le blocBuilder, car il peut être appelé plusieurs fois.
            //Alors que le Listener n'est appélé qu'une fois à chaque changement d'état.
            child: BlocConsumer<WeatherBloc, WeatherState>(
              listener: (context, state) {
                if (state is WeatherError) {
                  //On récupère donc le Scaffold, et on lui ajoute la snackBar
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text(state.message),
                    ),
                  );
                }
              },
              builder: (context, state) {
                if (state is WeatherInitial) {
                  return buildInitialInput();
                } else if (state is WeatherLoading) {
                  return buildLoading();
                } else if (state is WeatherLoaded) {
                  return buildColumnWithData(context, state.weather);
                } else if (state is WeatherError) {
                  return buildInitialInput();
                }
              },
            ),
          ),
          BlocConsumer<TimerBloc, TimerState>(builder: (context, state) {
            if (state is TimerRunning) {
              return Center(
                child: Column(
                  children: <Widget>[
                    Text(
                      '${state.clock.count} en cours',
                    ),
                    RaisedButton(
                      onPressed: () => context.bloc<TimerBloc>().add(
                            StopClock(),
                          ),
                      child: Text(
                        'Stop',
                      ),
                    )
                  ],
                ),
              );
            } else if (state is TimerStart) {
              return Center(
                child: Text('On démarre !'),
              );
            } else if (state is TimerStop) {
              return Center(
                child: Text('${state.clock.count} stoppé'),
              );
            }
            return Center(
              child: Text('J\'ai pas démarré !'),
            );
          }, listener: (context, state) {
            if (state is TimerStart) {
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text('Launching clock !'),
                ),
              );
            }
          })
        ],
      ),
    );
  }

  Widget buildInitialInput() {
    print('Build Initial');
    return Center(
      child: CityInputField(),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Column buildColumnWithData(BuildContext context, Weather weather) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Text(
          weather.cityName,
          style: TextStyle(
            fontSize: 40,
            fontWeight: FontWeight.w700,
          ),
        ),
        Text(
          // Display the temperature with 1 decimal place
          "${weather.temperatureCelsius.toStringAsFixed(1)} °C",
          style: TextStyle(fontSize: 80),
        ),
        RaisedButton(
          child: Text('See Details'),
          color: Colors.lightBlue[100],
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => BlocProvider.value(
                  value: BlocProvider.of<WeatherBloc>(context),
                  child: WeatherDetailPage(
                    masterWeather: weather,
                  ),
                ),
              ),
            );
          },
        ),
        CityInputField(),
      ],
    );
  }
}

class CityInputField extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 50),
      child: TextField(
        onSubmitted: (value) => submitCityName(context, value),
        textInputAction: TextInputAction.search,
        decoration: InputDecoration(
          hintText: "Enter a city",
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(12)),
          suffixIcon: Icon(Icons.search),
        ),
      ),
    );
  }

  //On doit gérer l'input utilisateur, donc, on récup le bloc du context, pour ensuite, grâce à lui, envoyer un event
  void submitCityName(BuildContext context, String cityName) {
    final weatherBloc = context.bloc<WeatherBloc>();
    weatherBloc.add(GetWeather(cityName));
    final timerBloc = context.bloc<TimerBloc>();
    timerBloc.add(StartClock());
  }
}
